use std::io;

fn main() {
    println!("Bienvenido a la Calculadora de Calificaciones");

    let mut activities: Vec<(f64, f64)> = Vec::new();

    loop {
        println!("Ingrese la nota de la actividad (o 'T' para salir):");

        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Error al leer la entrada");

        if input.trim() == "T" {
            break;
        }

        let nota: f64 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("¡Error! Por favor, ingrese un número válido.");
                continue;
            }
        };

        println!("Ingrese el peso de la actividad:");
        let mut peso_input = String::new();
        io::stdin().read_line(&mut peso_input).expect("Error al leer la entrada");

        let peso: f64 = match peso_input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("¡Error! Por favor, ingrese un número válido.");
                continue;
            }
        };

        activities.push((nota, peso));
    }

    if activities.is_empty() {
        println!("No se ingresaron datos. Saliendo del programa.");
    } else {
        let mut suma_productos = 0.0;
        let mut suma_pesos = 0.0;

        for &(nota, peso) in &activities {
            suma_productos += nota * peso;
            suma_pesos += peso;
        }

        let promedio_ponderado = suma_productos / suma_pesos;

        println!("El promedio ponderado de las actividades es: {:.2}", promedio_ponderado);
    }
}
