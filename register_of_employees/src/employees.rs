pub struct Employee {
    pub name: String,
    pub age: u32,
    pub salary: f64,
    pub position: String,
}

impl Employee {
    pub fn new(name: String, age: u32, salary: f64, position: String) -> Self {
        Employee { name, age, salary, position }
    }
}

pub struct EmployeeRegistration {
    pub employees: Vec<Employee>,
}

impl EmployeeRegistration {
    pub fn new() -> Self {
        EmployeeRegistration { employees: Vec::new() }
    }

    pub fn add_employee(&mut self, employee: Employee) {
        self.employees.push(employee);
    }

    pub fn show_employee(&self) {
        println!("Empleados registrados:");
        for employee in &self.employees {
            println!("Nombre: {}, Edad: {}, Salario: {}, Posición: {}", employee.name, employee.age, employee.salary, employee.position);
        }
    }

    pub fn search_employee(&self, name: &str) -> Option<&Employee> {
        self.employees.iter().find(|&e| e.name == name)
    }
}
