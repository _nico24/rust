mod employees;
mod utils;

use employees::{Employee, EmployeeRegistration};
use utils::enter_data;
use std::io;

fn main() {
    
    let mut register = EmployeeRegistration::new();

    loop {
        println!("\nMenú:");
        println!("1. Agregar empleado");
        println!("2. Mostrar todos los empleados");
        println!("3. Buscar empleado por nombre");
        println!("4. Salir");

        let mut option = String::new();
        io::stdin().read_line(&mut option).expect("Error al leer la entrada");

        match option.trim().parse::<u32>() {
            Ok(1) => {
                let name = enter_data("nombre");
                let age = enter_data("edad").parse().unwrap();
                let salary = enter_data("salario").parse().unwrap();
                let position = enter_data("posición");
                register.add_employee(Employee::new(name, age, salary, position));
            }
            Ok(2) => register.show_employee(),
            Ok(3) => {
                let search_name = enter_data("nombre");
                match register.search_employee(&search_name) {
                    Some(employee) => println!("Empleado encontrado - Nombre: {}, Edad: {}, Salario: {}, Posición: {}", employee.name, employee.age, employee.salary, employee.position),
                    None => println!("No se encontró ningún empleado con el nombre {}", search_name),
                }
            }
            Ok(4) => break,
            _ => println!("Opción inválida, por favor elige una opción válida."),
        }
    }
}
