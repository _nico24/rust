use std::io;

pub fn enter_data(nombre: &str) -> String {
    println!("Ingrese el {}:", nombre);
    let mut data = String::new();
    io::stdin().read_line(&mut data).expect("Error al leer la entrada");
    data.trim().to_string()
}
